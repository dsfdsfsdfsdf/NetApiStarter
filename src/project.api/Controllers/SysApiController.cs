using Autowired.Core;
using CoreHelper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using project.api.Models;
using project.api.Services;

namespace project.api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("[controller]/[action]")]
    [ApiController]
    public class SysApiController
    {
        [Autowired] SysService sysService;

        public SysApiController(AutowiredService autowiredService)
        {
            autowiredService.Autowired(this);
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ResultObject<SysLoginResponse> SysLogin([FromBody]SysLoginRequest request)
        {
            return sysService.SysLogin(request);
        }

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultObject EditUser([FromBody]SysEditUserRequest request)
        {
            return sysService.EditUser(request);
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultObject SysResetPassword([FromBody]SysResetPasswordRequest request)
        {
            return sysService.SysResetPassword(request);
        }

        /// <summary>
        /// 查询用户
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultObject<SysUserListResponse> SysUserList([FromBody]SysUserListRequest request)
        {
            return sysService.SysUserList(request);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultObject UpdatePassword([FromBody]SysUpdatePasswordRequest request)
        {
            return sysService.UpdatePassword(request);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultObject SysDeleteUser([FromBody]SysIdRequest request)
        {
            return sysService.SysDeleteUser(request);
        }

        /// <summary>
        /// 获取菜单
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultObject<SysMenuDataResponse> SysMenuData([FromBody]SysMenuDataRequest request)
        {
            return sysService.SysMenuData(request);
        }

        /// <summary>
        /// 编辑菜单
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultObject SysEditRes([FromBody]SysEditResRequest request)
        {
            return sysService.SysEditRes(request);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultObject SysDeleteRes([FromBody]SysIdRequest request)
        {
            return sysService.SysDeleteRes(request);
        }
        
        
    }
}